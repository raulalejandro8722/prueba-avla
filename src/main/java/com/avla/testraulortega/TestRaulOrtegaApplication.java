package com.avla.testraulortega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestRaulOrtegaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestRaulOrtegaApplication.class, args);
	}

}
