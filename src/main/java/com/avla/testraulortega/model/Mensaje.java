package com.avla.testraulortega.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Mensaje {

    private String mensaje;
}
