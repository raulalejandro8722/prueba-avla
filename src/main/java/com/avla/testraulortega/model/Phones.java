package com.avla.testraulortega.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Phones implements Serializable {
    private String number;
}
