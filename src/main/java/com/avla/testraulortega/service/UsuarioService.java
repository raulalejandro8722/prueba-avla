package com.avla.testraulortega.service;

import com.avla.testraulortega.model.Usuario;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UsuarioService {
    Usuario saveUsuario(Usuario usuario);

    List<Usuario> findAllUsuarios();

    void deleteUsuario(Long usuarioId);

    Optional<Usuario> findById(Long id);

    Usuario updateUsuario(Usuario usuario);

    boolean existsByEmail(String email);

    boolean existsById(Long id);
}
