para iniciar el proyecto con docker realizar los siguientes comandos.

gradlew clean build -x test

docker image build -t app .

docker images (para ver id de la imagen)

docker run -d -p 6565:6565 image-id(id de comando anterior)

el puerto de la aplicacion es 6565

para ver la documentacion de swagger ingresar a:

http://localhost:6565/swagger-ui/index.html